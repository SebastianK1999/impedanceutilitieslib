#ifndef MACROS_INCLUDED
#define MACROS_INCLUDED

// CONSTEXPR_T
#ifdef __cplusplus
    #define CONSTEXPR_T constexpr
#else
    #define CONSTEXPR_T
#endif // __cplusplus

// CONSTEXPR_OR_STATIC
#ifdef __cplusplus
    #define CONSTEXPR_OR_STATIC constexpr
#else
    #define CONSTEXPR_OR_STATIC static
#endif // __cplusplus

// IASSERT(condition, message)
#define IASSERT(condition, message) assert(((void) message , condition ));

// COUNT_CONTENT(...)
#ifdef __cplusplus
    #include <tuple>
    #define COUNT_CONTENT(...) (std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value)
#else // __cplusplus
    #define COUNT_CONTENT(...) (sizeof((int[]){__VA_ARGS__})/sizeof(int))
#endif // __cplusplus

// EXTERN_C
#ifdef __cplusplus
    #define EXTERN_C extern "C"
#else
    #define EXTERN_C extern
#endif

#endif // MACROS_INCLUDED
